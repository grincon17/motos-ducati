import { Injectable } from '@angular/core';

export interface Message {
  bikeId: number;
  bikeBrand: string;
  bikeModel: string;
  bikeYear: number;
  bikeImage: string;
  bikePrice: number;
  bikeContent: string;
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public message: Message[] = [
    {
      bikeId: 1,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati 950 Hypermotard',
      bikeYear: 2017,
      bikeImage: 'https://www.cuidamostumoto.com/sites/default/files/ducati-hypermotard-950-2019.jpg',
      bikePrice: 16890,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Once upon a midnight dreary, while I pondered, weak and weary,\n' +
          'Over many a quaint and curious volume of forgotten lore—\n' +
          '    While I nodded, nearly napping, suddenly there came a tapping,\n' +
          'As of some one gently rapping, rapping at my chamber door.\n' +
          '“’Tis some visitor,” I muttered, “tapping at my chamber door—\n' +
          '            Only this and nothing more.”'
    },
    {
      bikeId: 2,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Monster 797 Plus',
      bikeYear: 2019,
      bikeImage: 'https://www.cuidamostumoto.com/sites/default/files/ducati_monster-797_2019.jpg',
      bikePrice: 9090,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Ah, distinctly I remember it was in the bleak December;\n' +
          'And each separate dying ember wrought its ghost upon the floor.\n' +
          '    Eagerly I wished the morrow;—vainly I had sought to borrow\n' +
          '    From my books surcease of sorrow—sorrow for the lost Lenore—\n' +
          'For the rare and radiant maiden whom the angels name Lenore—\n' +
          '            Nameless here for evermore.'
    },
    {
      bikeId: 3,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Scrambler 1100',
      bikeYear: 2018,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-scrambler-1100.jpg',
      bikePrice: 13190,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'And the silken, sad, uncertain rustling of each purple curtain\n' +
          'Thrilled me—filled me with fantastic terrors never felt before;\n' +
          '    So that now, to still the beating of my heart, I stood repeating'
    },
    {
      bikeId: 4,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Panigale V4',
      bikeYear: 2018,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-panigale-v4-2018-2.jpg',
      bikePrice: 17890,
      // tslint:disable-next-line:max-line-length
      bikeContent: '“’Tis some visitor entreating entrance at my chamber door—\n' +
          'Some late visitor entreating entrance at my chamber door;—\n' +
          '            This it is and nothing more.”'
    },
    {
      bikeId: 5,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Multistrada 1260',
      bikeYear: 2018,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-multistrada-1260-2018.jpg',
      bikePrice: 17890,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Presently my soul grew stronger; hesitating then no longer,\n' +
          '“Sir,” said I, “or Madam, truly your forgiveness I implore;\n' +
          '    But the fact is I was napping, and so gently you came rapping,\n' +
          '    And so faintly you came tapping, tapping at my chamber door,\n' +
          'That I scarce was sure I heard you”—here I opened wide the door;—\n' +
          '            Darkness there and nothing more.'
    },
    {
      bikeId: 6,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Scrambler Icon',
      bikeYear: 2016,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-scrambler-icon.jpg',
      bikePrice: 8790,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Deep into that darkness peering, long I stood there wondering, fearing,\n' +
          'Doubting, dreaming dreams no mortal ever dared to dream before;\n' +
          '    But the silence was unbroken, and the stillness gave no token,\n' +
          '    And the only word there spoken was the whispered word, “Lenore?”\n' +
          'This I whispered, and an echo murmured back the word, “Lenore!”—\n' +
          '            Merely this and nothing more.'
    },
    {
      bikeId: 7,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Scrambler Classic',
      bikeYear: 2016,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati_scrambler_classic_2016.jpg',
      bikePrice: 10350,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Back into the chamber turning, all my soul within me burning,\n' +
          'Soon again I heard a tapping somewhat louder than before.\n' +
          '    “Surely,” said I, “surely that is something at my window lattice;\n' +
          '      Let me see, then, what thereat is, and this mystery explore—\n' +
          'Let my heart be still a moment and this mystery explore;—\n' +
          '            ’Tis the wind and nothing more!”'
    },
    {
      bikeId: 8,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Panigale V4 25 Aniversario 916',
      bikeYear: 2020,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-panigale-v4-25-aniversario-916.jpg',
      bikePrice: 48000,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Open here I flung the shutter, when, with many a flirt and flutter,\n' +
          'In there stepped a stately Raven of the saintly days of yore;\n' +
          '    Not the least obeisance made he; not a minute stopped or stayed he;'
    },
    {
      bikeId: 9,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Panigale V4 S',
      bikeYear: 2020,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-panigale-v4-s-2020.jpg',
      bikePrice: 31590,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'But, with mien of lord or lady, perched above my chamber door—\n' +
          'Perched upon a bust of Pallas just above my chamber door—\n' +
          '            Perched, and sat, and nothing more.'
    },
    {
      bikeId: 10,
      bikeBrand: 'Ducati',
      bikeModel: 'DUCATI MONSTER 1200',
      bikeYear: 2014 ,
      bikeImage: 'https://a.mcdn.es/mnet_ft//DUCATI/MONSTER_1200/30135MG.jpg/660x/',
      bikePrice: 13650,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Then this ebony bird beguiling my sad fancy into smiling,\n' +
          'By the grave and stern decorum of the countenance it wore,\n' +
          '“Though thy crest be shorn and shaven, thou,” I said, “art sure no craven,\n' +
          'Ghastly grim and ancient Raven wandering from the Nightly shore—\n' +
          'Tell me what thy lordly name is on the Night’s Plutonian shore!”\n' +
          '            Quoth the Raven “Nevermore.”'
    },
    {
      bikeId: 11,
      bikeBrand: 'Ducati',
      bikeModel: 'MONSTER 1200',
      bikeYear: 2018,
      bikeImage: 'https://a.mcdn.es/mnet_ft//DUCATI/MONSTER_1200/6164/30075MG.jpg/660x/',
      bikePrice: 20090,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Much I marvelled this ungainly fowl to hear discourse so plainly,\n' +
          'Though its answer little meaning—little relevancy bore;\n' +
          '    For we cannot help agreeing that no living human being\n' +
          '    Ever yet was blessed with seeing bird above his chamber door—\n' +
          'Bird or beast upon the sculptured bust above his chamber door,\n' +
          '            With such name as “Nevermore.”'
    },
    {
      bikeId: 12,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Monster 1200 S Black on Black',
      bikeYear: 2020,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-monster-1200-s-black-on-black-4.jpg',
      bikePrice: 18090,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'But the Raven, sitting lonely on the placid bust, spoke only\n' +
          'That one word, as if his soul in that one word he did outpour.\n' +
          '    Nothing farther then he uttered—not a feather then he fluttered—\n' +
          '    Till I scarcely more than muttered “Other friends have flown before—\n' +
          'On the morrow he will leave me, as my Hopes have flown before.”\n' +
          '            Then the bird said “Nevermore.”'
    },
    {
      bikeId: 13,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Scrambler Café Racer',
      bikeYear: 2019,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-scrambler-cafe-racer-2019.jpg',
      bikePrice: 11690,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'Startled at the stillness broken by reply so aptly spoken,\n' +
          '“Doubtless,” said I, “what it utters is its only stock and store\n' +
          '    Caught from some unhappy master whom unmerciful Disaster\n' +
          '    Followed fast and followed faster till his songs one burden bore—\n' +
          'Till the dirges of his Hope that melancholy burden bore\n' +
          '            Of ‘Never—nevermore’.”'
    },
    {
      bikeId : 14,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Scrambler Desert Sled',
      bikeYear: 2019,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-scrambler-desert-sled-2019-2.jpg',
      bikePrice: 11790,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'But the Raven still beguiling all my fancy into smiling,\n' +
          'Straight I wheeled a cushioned seat in front of bird, and bust and door;\n' +
          '    Then, upon the velvet sinking, I betook myself to linking\n' +
          '    Fancy unto fancy, thinking what this ominous bird of yore—\n' +
          'What this grim, ungainly, ghastly, gaunt, and ominous bird of yore\n' +
          '            Meant in croaking “Nevermore.”\n'
    },
    {
      bikeId: 15,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Multistrada 1260',
      bikeYear: 2018,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-multistrada-1260-2018.jpg',
      bikePrice: 17890,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'This I sat engaged in guessing, but no syllable expressing\n' +
          'To the fowl whose fiery eyes now burned into my bosom’s core;\n' +
          '    This and more I sat divining, with my head at ease reclining\n' +
          '    On the cushion’s velvet lining that the lamp-light gloated o’er,\n' +
          'But whose velvet-violet lining with the lamp-light gloating o’er,\n' +
          '            She shall press, ah, nevermore!'
    },
    {
      bikeId: 16,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati Hypermotard',
      bikeYear: 2018,
      bikeImage: 'https://a.mcdn.es/mnet_ft//DUCATI/HYPERMOTARD/6736/34138MG.jpg/660x/',
      bikePrice: 13690,
      // tslint:disable-next-line:max-line-length
      bikeContent: ' Then, methought, the air grew denser, perfumed from an unseen censer\n' +
          'Swung by Seraphim whose foot-falls tinkled on the tufted floor.\n' +
          '    “Wretch,” I cried, “thy God hath lent thee—by these angels he hath sent thee\n' +
          '    Respite—respite and nepenthe from thy memories of Lenore;\n' +
          'Quaff, oh quaff this kind nepenthe and forget this lost Lenore!”\n' +
          '            Quoth the Raven “Nevermore.”'
    },
    {
      bikeId: 17,
      bikeBrand: 'Ducati',
      bikeModel: 'Ducati 950 Hypermotard 2019',
      bikeYear: 2019,
      bikeImage: 'https://a.mcdn.es/mnet_ft//DUCATI/HYPERMOTARD/6736/34138MG.jpg/660x/',
      bikePrice: 16890,
      // tslint:disable-next-line:max-line-length
      bikeContent: '“Prophet!” said I, “thing of evil!—prophet still, if bird or devil!—\n' +
          'Whether Tempter sent, or whether tempest tossed thee here ashore,\n' +
          '    Desolate yet all undaunted, on this desert land enchanted—\n' +
          '    On this home by Horror haunted—tell me truly, I implore—\n' +
          'Is there—is there balm in Gilead?—tell me—tell me, I implore!”\n' +
          '            Quoth the Raven “Nevermore.”'
    },
    {
      bikeId: 18,
      bikeBrand: 'Ducati',
      bikeModel: 'Diavel',
      bikeYear: 2019,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-diavel-1260-2019.jpg',
      bikePrice: 16890,
      // tslint:disable-next-line:max-line-length
      bikeContent: '“Prophet!” said I, “thing of evil!—prophet still, if bird or devil!\n' +
          'By that Heaven that bends above us—by that God we both adore—\n' +
          '    Tell this soul with sorrow laden if, within the distant Aidenn,\n' +
          '    It shall clasp a sainted maiden whom the angels name Lenore—\n' +
          'Clasp a rare and radiant maiden whom the angels name Lenore.”\n' +
          '            Quoth the Raven “Nevermore.”'
    },
    {
      bikeId: 19,
      bikeBrand: 'Ducati',
      bikeModel: '1260 Diavel S',
      bikeYear: 2020,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-diavel-1260-s-2019.jpg',
      bikePrice: 23590,
      // tslint:disable-next-line:max-line-length
      bikeContent: '“Be that word our sign of parting, bird or fiend!” I shrieked, upstarting—\n' +
          '“Get thee back into the tempest and the Night’s Plutonian shore!\n' +
          '    Leave no black plume as a token of that lie thy soul hath spoken!\n' +
          '    Leave my loneliness unbroken!—quit the bust above my door!\n' +
          'Take thy beak from out my heart, and take thy form from off my door!”\n' +
          '            Quoth the Raven “Nevermore.”'
    },
    {
      bikeId: 20,
      bikeBrand: 'Ducati',
      bikeModel: 'Scrambler Icon 2019',
      bikeYear: 2020,
      bikeImage: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-scrambler-icon-2019.jpg',
      bikePrice: 9190,
      // tslint:disable-next-line:max-line-length
      bikeContent: 'And the Raven, never flitting, still is sitting, still is sitting\n' +
          'On the pallid bust of Pallas just above my chamber door;\n' +
          '    And his eyes have all the seeming of a demon’s that is dreaming,\n' +
          '    And the lamp-light o’er him streaming throws his shadow on the floor;\n' +
          'And my soul from out that shadow that lies floating on the floor\n' +
          '            Shall be lifted—nevermore!'
    }
  ];

  constructor() { }

  public getMessage(): Message[] {
    return this.message;
  }

  public getMessageById(id: number): Message {
    return this.message[id - 1];
  }

}
