import { Component, OnInit } from '@angular/core';
import {DataService, Message} from '../services/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-message-detail',
  templateUrl: './message-detail.page.html',
  styleUrls: ['./message-detail.page.scss'],
})
export class MessageDetailPage implements OnInit {

  public message: Message;

  constructor(private data: DataService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    // tslint:disable-next-line:radix
    this.message = this.data.getMessageById(parseInt(id));
  }

}
